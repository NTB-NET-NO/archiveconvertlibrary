Imports System.Xml
Imports System.text
Imports System.IO

Public Class ConvertLibrary

    'Structure to hold Xml Article information
    Public Class xmlArticleHeader
        Implements IComparable

        Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo
            'Use date for comparision when sorting
            Return timestamp.CompareTo(obj.timestamp)
        End Function

        Public id As String = New String("")
        Public version As Long
        Public longId As String = New String("")

        Public title As String = New String("") 'Long SO, ie with nyX and /F
        Public stikkord As String = New String("") 'Short SO, ie nyX and /F stripped off

        Public type As String = New String("")
        Public signature As String = New String("")
        Public stoffGruppe As String = New String("")
        Public underGruppe As String = New String("")

        Public target As String = New String("")
        Public distCode As String = New String("")
        Public timestamp As Date

        Public correction As String = New String("")

        Public fileName As String = New String("")
        Public fileFolder As String = New String("")

        Public xmlDoc As XmlDocument = New XmlDocument
        Public NITFVersion As Integer
    End Class

    'Load headers for the 2.5 NITF format
    Protected Shared Sub LoadHdr25(ByRef hdrs As xmlArticleHeader)

        Dim ver As Integer

        hdrs.NITFVersion = 25

        hdrs.longId = hdrs.xmlDoc.SelectSingleNode("/nitf/head/docdata/doc-id/@id-string").Value
        hdrs.title = hdrs.xmlDoc.SelectSingleNode("/nitf/head/title").InnerText

        hdrs.stikkord = hdrs.title.Split("/")(0)

        Dim rx As RegularExpressions.Regex = New RegularExpressions.Regex("^ny\d{1,2}-", RegularExpressions.RegexOptions.IgnoreCase)
        If rx.IsMatch(hdrs.stikkord) Or hdrs.stikkord.StartsWith("HAST") Then
            hdrs.stikkord = hdrs.stikkord.Substring(hdrs.stikkord.IndexOf("-") + 1)
        End If

        hdrs.stoffGruppe = hdrs.xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").Value
        hdrs.underGruppe = hdrs.xmlDoc.SelectSingleNode("/nitf/head/tobject/tobject.property/@tobject.property.type").Value

        hdrs.type = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-meldingsType']/@content").Value
        hdrs.signature = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-meldingsSign']/@content").Value

        hdrs.timestamp = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-date']/@content").Value
        hdrs.id = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-id']/@content").Value
        ver = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-meldingsVersjon']/@content").Value
        hdrs.target = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-folder']/@content").Value
        hdrs.distCode = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-distribusjonsKode']/@content").Value

        'Checking version format/presence
        If IsNumeric(ver) Then
            hdrs.version = ver
        ElseIf hdrs.longId <> "" Then
            hdrs.version = hdrs.longId.Substring(hdrs.longId.LastIndexOf("_") + 1)
        End If

        'Error checking, missing info
        If hdrs.longId = "00" And hdrs.id <> "" Then
            hdrs.longId = hdrs.id
        ElseIf hdrs.longId = "" Then
            hdrs.longId = Path.GetFileNameWithoutExtension(hdrs.fileName)
        End If

        If hdrs.id = "" Then hdrs.id = hdrs.longId

    End Sub

    'Load headers for the 3.2 NITF format
    Protected Shared Sub LoadHdr32(ByRef hdrs As xmlArticleHeader)

        Dim ver As Integer

        hdrs.NITFVersion = 32

        hdrs.longId = hdrs.xmlDoc.SelectSingleNode("/nitf/head/docdata/doc-id/@id-string").Value
        ver = hdrs.xmlDoc.SelectSingleNode("/nitf/head/docdata/du-key/@version").Value

        hdrs.stikkord = hdrs.xmlDoc.SelectSingleNode("/nitf/head/docdata/du-key/@key").Value
        If hdrs.stikkord.StartsWith("HAST") Then
            hdrs.stikkord = hdrs.stikkord.Substring(hdrs.stikkord.IndexOf("-") + 1)
        End If

        hdrs.stoffGruppe = hdrs.xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").Value
        hdrs.underGruppe = hdrs.xmlDoc.SelectSingleNode("/nitf/head/tobject/tobject.property/@tobject.property.type").Value

        hdrs.title = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='subject']/@content").Value
        hdrs.type = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBMeldingsType']/@content").Value
        hdrs.signature = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBMeldingsSign']/@content").Value

        hdrs.timestamp = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-dato']/@content").Value
        hdrs.id = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBID']/@content").Value

        hdrs.target = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='foldername']/@content").Value
        hdrs.distCode = hdrs.xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBDistribusjonsKode']/@content").Value

        'Checking version format/presence
        If IsNumeric(ver) Then
            hdrs.version = ver
        ElseIf hdrs.longId <> "" Then
            hdrs.version = hdrs.longId.Substring(hdrs.longId.LastIndexOf("_") + 1)
        End If

        If hdrs.id = "" Then hdrs.id = hdrs.longId

    End Sub


    'Loads the headers from file 
    Public Shared Sub LoadArticleHeaders(ByVal file As String, ByRef hdrs As xmlArticleHeader)

        hdrs = New xmlArticleHeader
        Dim xmlVer As String

        'Load XML
        hdrs.xmlDoc.Load(file)
        xmlVer = hdrs.xmlDoc.SelectSingleNode("/nitf/@version").Value

        'Loading XML data
        hdrs.fileName = Path.GetFileName(file)
        hdrs.fileFolder = Path.GetDirectoryName(file) & Path.DirectorySeparatorChar

        If xmlVer.IndexOf("2.5") > -1 Then
            LoadHdr25(hdrs)
        ElseIf xmlVer.IndexOf("3.2") > -1 Then
            LoadHdr32(hdrs)
        Else
            'Major error!!
        End If

    End Sub

    'Loads the headers from given XML document, ie no files on disk 
    Public Shared Sub LoadArticleHeaders(ByVal xmlDoc As XmlDocument, ByRef hdrs As xmlArticleHeader)

        hdrs = New xmlArticleHeader
        Dim xmlVer As String

        'Load XML
        hdrs.xmlDoc.LoadXml(xmlDoc.InnerXml)
        xmlVer = hdrs.xmlDoc.SelectSingleNode("/nitf/@version").Value

        'Loading XML data
        If xmlVer.IndexOf("2.5") > -1 Then
            LoadHdr25(hdrs)
        ElseIf xmlVer.IndexOf("3.2") > -1 Then
            LoadHdr32(hdrs)
        Else
            'Major error!!
        End If

    End Sub

    'Transform an article given by filename by reloading the Header structure and call DoTransform
    Public Shared Function TransformArticle(ByVal file As String) As String

        Dim hdrs As xmlArticleHeader

        'Load header
        LoadArticleHeaders(file, hdrs)

        'Transform and write to target
        If hdrs.NITFVersion = 25 Then
            Return DoTransform25(hdrs)
        ElseIf hdrs.NITFVersion = 32 Then
            Return DoTransform32(hdrs)
        End If

        Return String.Empty

    End Function

    'Transform an article given by the header structure
    Public Shared Function TransformArticle(ByVal hdrs As xmlArticleHeader) As String

        'Transform and write to target
        If hdrs.NITFVersion = 25 Then
            Return DoTransform25(hdrs)
        ElseIf hdrs.NITFVersion = 32 Then
            Return DoTransform32(hdrs)
        End If

        Return String.Empty
    End Function

    'Takes XML an NITF XML Header Structure and body and transforms the article to the archive format
    Private Shared Function DoTransform32(ByVal hdr As xmlArticleHeader) As String

        Dim nodeList As XmlNodeList
        Dim node As XmlNode

        'Extract headers

        'Stoffgrupper, SG
        Dim stoffGruppe As String = hdr.stoffGruppe
        Dim underGruppe As String = hdr.underGruppe

        Dim sg As String
        sg = stoffGruppe.Substring(0, 3).ToUpper()

        'Number, NR
        Dim nr As String
        Try
            nr = String.Format("{0}{1}-{2}", sg, hdr.id.Split("_")(1), hdr.timestamp.ToString("yyMMddHHmm"))
        Catch
            nr = String.Format("{0}{1}-{2}", sg, hdr.longId, hdr.timestamp.ToString("yyMMddHHmm"))
        End Try


        'Date/time DA-UT
        Dim d As String = hdr.timestamp.ToString("yy-MM-dd")
        Dim t As String = hdr.timestamp.ToString("HH:mm")

        'Stikkord, SO
        Dim so As String = hdr.title

        'Signature, SI
        Dim si As String = hdr.signature

        'Geography, GE
        Dim ge As String = New String("")

        nodeList = hdr.xmlDoc.SelectNodes("/nitf/head/docdata/evloc")
        For Each node In nodeList
            Dim state As String = ""
            Dim county As String = ""

            Try
                state = node.Attributes("state-prov").Value
                county = node.Attributes("county-dist").Value
            Catch ex As Exception
            End Try

            If state <> "Global" And county = "" Then
                ge &= state & ";"
            ElseIf county <> "" And county <> "Riksnyheter" Then
                ge &= county & ";"
            End If
        Next

        'Add "Norge;" if not set for "Dagen i dag" and "Jubilant-omtaler"
        If (stoffGruppe.StartsWith("Dagen") Or stoffGruppe.StartsWith("Jubilant")) And _
        ge.IndexOf("Norge;") = -1 Then ge &= "Norge;"

        'Variables for classification
        Dim es As String = New String("")
        Dim kat As String = New String("")
        Dim eo As String = New String("")

        'Set ES based on stoffgruppe
        Select Case stoffGruppe
            Case "Dagen i dag"
                es &= "Dagen i dag;"
            Case "Feature"
                es &= "Feature;"
            Case "Jubilant-omtaler"
                es &= "F�dselsdag;Personalia;"
            Case "Ungdom"
                es &= "Kultur;Befolkning;"
            Case "Kultur og underholdning"
                es &= "Kultur;"
        End Select

        'Set ES based on undergruppe
        If underGruppe = "Bakgrunn" Or underGruppe = "Analyse" Then
            Select Case stoffGruppe
                Case "Utenriks"
                    es &= "Utenriks bakgrunn;"
                Case "Innenriks"
                    es &= "Innenriks bakgrunn;"
                Case "Sport"
                    es &= "Sport bakgrunn;"
            End Select
        ElseIf underGruppe.StartsWith("Fakta") Then
            es &= "Fakta;"
        ElseIf underGruppe = "Feature" Then
            es &= "Feature;"
        ElseIf underGruppe = "Notiser" Then
            es &= "Notiser;"
        ElseIf underGruppe.StartsWith("Profiler") Then
            es &= "Personalia;Profil;"
        End If

        'Set EO based on stoffgruppe (NTB Pluss specific)
        If stoffGruppe.StartsWith("Kultur") Or stoffGruppe.StartsWith("Feature") Or _
        stoffGruppe.StartsWith("Reiser") Or stoffGruppe.StartsWith("Helse") Or _
        stoffGruppe.StartsWith("PC") Or stoffGruppe.StartsWith("Moter") Or _
        stoffGruppe.StartsWith("Forbruker") Or stoffGruppe.StartsWith("Dagen") Or _
        stoffGruppe.StartsWith("Jubilant") Then
            eo &= "Spesialtjeneste;"
        End If

        'Set EO based on undergruppe
        If underGruppe.StartsWith("Tabeller") Then
            eo &= "Tabeller;Resultater;"
        ElseIf underGruppe = "Nekrologer" Then
            eo &= "Nekrologer;"
        End If

        'Set ES and EO based on IPTC categories/subcategories
        nodeList = hdr.xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject")
        For Each node In nodeList

            'Fetch IPTC categories/subcategories
            Dim cat As String = New String("")
            Dim cCode As String = New String("")
            Dim mtr As String = New String("")

            Try
                cCode = node.Attributes("tobject.subject.code").Value
                cat = node.Attributes("tobject.subject.type").Value
            Catch
                'No category, probably subcat... no problem :)
            End Try

            Try
                mtr = node.Attributes("tobject.subject.matter").Value
                mtr = mtr.ToUpper()
            Catch
                'No subcategory(matter) given
            End Try

            'Set KA
            If cat <> "" Then kat &= cat & ";"

            'These categorization rules were given by the old archiving dept. They are based on how they labeled 
            'the articles manually, ie. legacy rules
            If cCode = "KUL" Then
                'Handle ES and EO for category "Kultur og underholdning"
                If mtr.IndexOf("MUSIKK") > -1 Then eo &= "Musikk;"
                If mtr.IndexOf("FILM") > -1 Then eo &= "Film;"
                If mtr.IndexOf("LITTERATUR") > -1 Or mtr.IndexOf("Tegneserier") > -1 Then eo &= "Litteratur;"
                If mtr.IndexOf("RADIO") > -1 Then es &= "Media;"
            ElseIf cCode = "KRE" Then
                'Handle ES and EO for category "Kriminalitet og rettsvesen"
                es &= "Forbrytelser;"
            ElseIf cCode = "ULY" Then
                'Handle ES and EO for category "Ulykker og naturkatastrofer"
                es &= "Ulykker;"
            ElseIf cCode = "OKO" Then
                'Handle ES and EO for category "�konomi og n�ringsliv"
                If mtr.IndexOf("LANDBRUK") > -1 Then es &= "Landbruk;"
                If mtr.IndexOf("FISKERI") > -1 Then es &= "Fiske;"
                If mtr.IndexOf("IT") > -1 Or mtr.IndexOf("Samferdsel") > -1 Then es &= "Kommunikasjon;"
                If mtr.IndexOf("OLJE") > -1 Then es &= "Olje;"
                If mtr.IndexOf("BOLIG") > -1 Then es &= "Bo;"
                If mtr.IndexOf("BANK") > -1 Then es &= "Finans;"
                If mtr.IndexOf("MEDIER") > -1 Then es &= "Media;"
                If mtr.IndexOf("INDUSTRI") > -1 Then es &= "Industri;"
                If mtr.IndexOf("HANDEL") > -1 Then es &= "Handel;"
                If mtr.IndexOf("REISELIV") > -1 Then eo &= "Reiseliv;"
                If mtr.IndexOf("FORBRUKER") > -1 Then eo &= "Forbrukersp�rsm�l;"
            ElseIf cCode = "UTD" Then
                'Handle ES and EO for category "Utdanning"
                es &= "Utdannelse;"
            ElseIf cCode = "MED" Then
                'Handle ES and EO for category "Medisin og helse"
                es &= "Helse;"
            ElseIf cCode = "KUR" Then
                'Handle ES and EO for category "Kuriosa"
                If mtr.IndexOf("KONGESTOFF") > -1 Then es &= "Kongehuset;"
                If mtr.IndexOf("KURIOSA") > -1 Then eo &= "Kuriosa;"
            ElseIf cCode = "ARB" Then
                'Handle ES and EO for category "Arbeidsliv"
                es &= "Arbeidsliv;"
            ElseIf cCode = "FRI" Then
                'Handle ES and EO for category "Fritid"
                es &= "Fritid;"
            ElseIf cCode = "REL" Then
                'Handle ES and EO for category "Religion og livssyn"
                es &= "Religion;"
            ElseIf cCode = "VIT" Then
                'Handle ES and EO for category "Vitenskap og teknologi"
                es &= "Vitenskap;"
            ElseIf cCode = "SPO" Then
                'Handle ES and EO for category "Sport"
                'Add sports to EO, upcasing first letter
                If mtr <> "" Then
                    mtr = mtr.ToLower()
                    eo &= mtr.Substring(0, 1).ToUpper() & mtr.Substring(1) & ";"
                Else
                    es &= "Sport;"
                End If

            ElseIf cCode = "VAR" Then
                'Handle ES and EO for category "V�r"
                eo &= "V�r;"
            End If
        Next

        'Header output
        Dim output As StringBuilder = New StringBuilder(1024)

        output.Append("*NR:" & nr & vbCrLf)
        output.Append("*DA:" & d & vbCrLf)
        output.Append("*UT:" & t & vbCrLf)
        output.Append("*SG:" & sg & vbCrLf)
        output.Append("*SI:" & si & vbCrLf)
        output.Append("*SO:" & so & vbCrLf)

        output.Append("*GE:" & ge & vbCrLf)
        output.Append("*KA:" & kat & vbCrLf)
        output.Append("*ES:" & es & vbCrLf)
        output.Append("*EO:" & eo & vbCrLf)
        output.Append("*SA:" & vbCrLf)
        output.Append("*RE:" & hdr.correction & vbCrLf)

        'Extract article content

        'Article header
        output.Append("*TI:" & hdr.xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText & vbCrLf)

        'Article parts
        Dim by As String = ""
        Dim ingress As String = ""
        Dim text As String = ""

        Try
            by = hdr.xmlDoc.SelectSingleNode("/nitf/body/body.head/byline").InnerText & vbCrLf
        Catch
            'Catch missing byline
            by = ""
        End Try

        nodeList = hdr.xmlDoc.SelectNodes("/nitf/body/body.content/*")
        For Each node In nodeList

            If node.LocalName = "p" Then

                'Paragraph markers
                Dim lede As String
                Dim inn As String

                Try
                    lede = node.Attributes("lede").Value
                Catch
                    'Catch missing attribute
                    lede = ""
                End Try

                Try
                    inn = node.Attributes("innrykk").Value
                Catch
                    'Catch missing attribute
                    inn = ""
                End Try

                Dim txt As String = node.InnerText.Trim()
                If Not txt.StartsWith("[") Then

                    If lede = "" And inn = "" And ingress = "" And text = "" Then
                        text &= txt & vbCrLf
                    ElseIf ingress = "" And lede = "true" Then
                        ingress = txt & vbCrLf
                    ElseIf inn = "true" And text <> "" Then
                        text &= "  " & txt & vbCrLf
                    Else
                        text &= txt & vbCrLf
                    End If
                End If

            ElseIf node.LocalName = "hl2" Then
                'Paragraph header
                If text = "" Then
                    text &= node.InnerText & vbCrLf
                Else
                    text &= vbCrLf & node.InnerText & vbCrLf
                End If

            ElseIf node.LocalName = "table" Then
                'Handle tables

                Dim tr As XmlNodeList = node.SelectNodes("*")
                Dim td As XmlNodeList
                Dim trNd, tdNd As XmlNode

                'Padding varies according to table type
                Dim lPad As Integer = 28
                Dim sPad As Integer = 4

                'loop through each <tr> and <td>
                For Each trNd In tr
                    td = trNd.SelectNodes("*")

                    'Control vars to get the columns right
                    Dim pad As Integer = 0
                    Dim sec As Integer = 1
                    For Each tdNd In td
                        If sec = 1 Then pad = lPad Else pad = sPad
                        text &= tdNd.InnerText.PadRight(pad)
                        sec += 1
                    Next

                    'New line
                    text &= vbCrLf
                Next
            Else
                'More optional tags handeled here
            End If

        Next 'End body handling

        'Print body parts
        If ingress <> "" Then output.Append("*IN:" & ingress)
        If by <> "" Then
            output.Append("*TE:" & by & text)
        Else
            output.Append("*TE:" & text)
        End If

        'Print article end marker
        output.Append(vbCrLf & vbCrLf & "*** BRS DOCUMENT BOUNDARY ***" & vbCrLf) ' & hdr.fileName

        Return output.ToString()
    End Function


    'Takes XML an NITF XML Header Structure and body and transforms the article to the archive format
    Private Shared Function DoTransform25(ByVal hdr As xmlArticleHeader) As String

        Dim nodeList As XmlNodeList
        Dim node As XmlNode

        'Extract headers

        'Stoffgrupper, SG
        Dim stoffGruppe As String = hdr.stoffGruppe
        Dim underGruppe As String = hdr.underGruppe

        Dim sg As String
        sg = stoffGruppe.Substring(0, 3).ToUpper()

        'Number, NR
        Dim nr As String
        Try
            nr = String.Format("{0}{1}-{2}", sg, hdr.id.Split("_")(1), hdr.timestamp.ToString("yyMMddHHmm"))
        Catch
            nr = String.Format("{0}{1}-{2}", sg, hdr.longId, hdr.timestamp.ToString("yyMMddHHmm"))
        End Try


        'Date/time DA-UT
        Dim d As String = hdr.timestamp.ToString("yy-MM-dd")
        Dim t As String = hdr.timestamp.ToString("HH:mm")

        'Stikkord, SO
        Dim so As String = hdr.title

        'Signature, SI
        Dim si As String = hdr.signature

        'Geography, GE
        Dim geoCountry As String = hdr.xmlDoc.SelectSingleNode("/nitf/head/docdata/evloc").InnerText
        Dim geoArea As String = hdr.xmlDoc.SelectSingleNode("/nitf/head/docdata/evloc/@county-dist").Value

        If geoCountry <> "" Then geoCountry = geoCountry.Substring(0, 1).ToUpper() & geoCountry.Substring(1)
        If geoArea <> "" Then geoArea = geoArea.Substring(0, 1).ToUpper() & geoArea.Substring(1)

        Dim ge As String = geoCountry
        If geoArea <> "" Then ge &= geoArea

        'Add "Norge;" if not set for "Dagen i dag" and "Jubilant-omtaler"
        If (stoffGruppe.StartsWith("Dagen") Or stoffGruppe.StartsWith("Jubilant")) And _
        ge.IndexOf("Norge;") = -1 Then ge &= "Norge;"

        'Remove some settings
        ge = ge.Replace("Global;", "")
        ge = ge.Replace("Riksnyheter;", "")

        'Variables for classification
        Dim es As String = New String("")
        Dim kat As String = New String("")
        Dim eo As String = New String("")

        'Set ES based on stoffgruppe
        Select Case stoffGruppe
            Case "Dagen i dag"
                es &= "Dagen i dag;"
            Case "Feature"
                es &= "Feature;"
            Case "Jubilant-omtaler"
                es &= "F�dselsdag;Personalia;"
            Case "Ungdom"
                es &= "Kultur;Befolkning;"
            Case "Kultur og underholdning"
                es &= "Kultur;"
        End Select

        'Set ES based on undergruppe
        If underGruppe = "Bakgrunn" Or underGruppe = "Analyse" Then
            Select Case stoffGruppe
                Case "Utenriks"
                    es &= "Utenriks bakgrunn;"
                Case "Innenriks"
                    es &= "Innenriks bakgrunn;"
                Case "Sport"
                    es &= "Sport bakgrunn;"
            End Select
        ElseIf underGruppe.StartsWith("Fakta") Then
            es &= "Fakta;"
        ElseIf underGruppe = "Feature" Then
            es &= "Feature;"
        ElseIf underGruppe = "Notiser" Then
            es &= "Notiser;"
        ElseIf underGruppe.StartsWith("Profiler") Then
            es &= "Personalia;Profil;"
        End If

        'Set EO based on stoffgruppe (NTB Pluss specific)
        If stoffGruppe.StartsWith("Kultur") Or stoffGruppe.StartsWith("Feature") Or _
        stoffGruppe.StartsWith("Reiser") Or stoffGruppe.StartsWith("Helse") Or _
        stoffGruppe.StartsWith("PC") Or stoffGruppe.StartsWith("Moter") Or _
        stoffGruppe.StartsWith("Forbruker") Or stoffGruppe.StartsWith("Dagen") Or _
        stoffGruppe.StartsWith("Jubilant") Then
            eo &= "Spesialtjeneste;"
        End If

        'Set EO based on undergruppe
        If underGruppe.StartsWith("Tabeller") Then
            eo &= "Tabeller;Resultater;"
        ElseIf underGruppe = "Nekrologer" Then
            eo &= "Nekrologer;"
        End If

        'Set ES and EO based on IPTC categories/subcategories
        nodeList = hdr.xmlDoc.SelectNodes("/nitf/head/tobject/tobject.subject")
        For Each node In nodeList

            'Fetch IPTC categories/subcategories
            Dim cat As String = node.Attributes("tobject.subject.type").Value
            Dim cCode As String = node.Attributes("tobject.subject.code").Value
            Dim mtr As String = New String("")
            Try
                mtr = node.Attributes("tobject.subject.matter").Value
                mtr = mtr.ToUpper()
            Catch
                'No subcategory(matter) given
                mtr = ""
            End Try

            'Set KA
            kat &= cat & ";"

            'These categorization rules were given by the old archiving dept. They are based on hov they labeled 
            'the articles manually, ie. legacy rules
            If cCode = "KUL" Then
                'Handle ES and EO for category "Kultur og underholdning"
                If mtr.IndexOf("MUSIKK") > -1 Then eo &= "Musikk;"
                If mtr.IndexOf("FILM") > -1 Then eo &= "Film;"
                If mtr.IndexOf("LITTERATUR") > -1 Or mtr.IndexOf("Tegneserier") > -1 Then eo &= "Litteratur;"
                If mtr.IndexOf("RADIO") > -1 Then es &= "Media;"
            ElseIf cCode = "KRE" Then
                'Handle ES and EO for category "Kriminalitet og rettsvesen"
                es &= "Forbrytelser;"
            ElseIf cCode = "ULY" Then
                'Handle ES and EO for category "Ulykker og naturkatastrofer"
                es &= "Ulykker;"
            ElseIf cCode = "OKO" Then
                'Handle ES and EO for category "�konomi og n�ringsliv"
                If mtr.IndexOf("LANDBRUK") > -1 Then es &= "Landbruk;"
                If mtr.IndexOf("FISKERI") > -1 Then es &= "Fiske;"
                If mtr.IndexOf("IT") > -1 Or mtr.IndexOf("Samferdsel") > -1 Then es &= "Kommunikasjon;"
                If mtr.IndexOf("OLJE") > -1 Then es &= "Olje;"
                If mtr.IndexOf("BOLIG") > -1 Then es &= "Bo;"
                If mtr.IndexOf("BANK") > -1 Then es &= "Finans;"
                If mtr.IndexOf("MEDIER") > -1 Then es &= "Media;"
                If mtr.IndexOf("INDUSTRI") > -1 Then es &= "Industri;"
                If mtr.IndexOf("HANDEL") > -1 Then es &= "Handel;"
                If mtr.IndexOf("REISELIV") > -1 Then eo &= "Reiseliv;"
                If mtr.IndexOf("FORBRUKER") > -1 Then eo &= "Forbrukersp�rsm�l;"
            ElseIf cCode = "UTD" Then
                'Handle ES and EO for category "Utdanning"
                es &= "Utdannelse;"
            ElseIf cCode = "MED" Then
                'Handle ES and EO for category "Medisin og helse"
                es &= "Helse;"
            ElseIf cCode = "KUR" Then
                'Handle ES and EO for category "Kuriosa"
                If mtr.IndexOf("KONGESTOFF") > -1 Then es &= "Kongehuset;"
                If mtr.IndexOf("KURIOSA") > -1 Then eo &= "Kuriosa;"
            ElseIf cCode = "ARB" Then
                'Handle ES and EO for category "Arbeidsliv"
                es &= "Arbeidsliv;"
            ElseIf cCode = "FRI" Then
                'Handle ES and EO for category "Fritid"
                es &= "Fritid;"
            ElseIf cCode = "REL" Then
                'Handle ES and EO for category "Religion og livssyn"
                es &= "Religion;"
            ElseIf cCode = "VIT" Then
                'Handle ES and EO for category "Vitenskap og teknologi"
                es &= "Vitenskap;"
            ElseIf cCode = "SPO" Then
                'Handle ES and EO for category "Sport"
                es &= "Sport;"
                'Add sports to EO, upcasing first letter
                If mtr <> "" Then eo &= mtr.Substring(0, 1).ToUpper() & mtr.Substring(1)
            ElseIf cCode = "VAR" Then
                'Handle ES and EO for category "V�r"
                eo &= "V�r;"
            End If
        Next

        'Header output
        Dim output As StringBuilder = New StringBuilder(1024)

        output.Append("*NR:" & nr & vbCrLf)
        output.Append("*DA:" & d & vbCrLf)
        output.Append("*UT:" & t & vbCrLf)
        output.Append("*SG:" & sg & vbCrLf)
        output.Append("*SI:" & si & vbCrLf)
        output.Append("*SO:" & so & vbCrLf)

        output.Append("*GE:" & ge & vbCrLf)
        output.Append("*KA:" & kat & vbCrLf)
        output.Append("*ES:" & es & vbCrLf)
        output.Append("*EO:" & eo & vbCrLf)
        output.Append("*SA:" & vbCrLf)
        output.Append("*RE:" & hdr.correction & vbCrLf)

        'Extract article content

        'Article header
        output.Append("*TI:" & hdr.xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText & vbCrLf)

        'Article parts
        Dim by As String = ""
        Dim ingress As String = ""
        Dim text As String = ""

        Try
            by = hdr.xmlDoc.SelectSingleNode("/nitf/body/body.head/byline").InnerText & vbCrLf
        Catch
            'Catch missing byline
            by = ""
        End Try

        nodeList = hdr.xmlDoc.SelectNodes("/nitf/body/body.content/*")
        For Each node In nodeList

            If node.LocalName = "p" Then

                'Paragraph markers
                Dim lede As String
                Dim inn As String

                Try
                    lede = node.Attributes("lede").Value
                Catch
                    'Catch missing attribute
                    lede = ""
                End Try

                Try
                    inn = node.Attributes("innrykk").Value
                Catch
                    'Catch missing attribute
                    inn = ""
                End Try

                Dim txt As String = node.InnerText.Trim()
                If Not txt.StartsWith("[") Then

                    If lede = "" And inn = "" And ingress = "" And text = "" Then
                        text &= txt & vbCrLf
                    ElseIf ingress = "" And lede = "true" Then
                        ingress = txt & vbCrLf
                    ElseIf inn = "true" And text <> "" Then
                        text &= "  " & txt & vbCrLf
                    Else
                        text &= txt & vbCrLf
                    End If
                End If

            ElseIf node.LocalName = "hl2" Then
                'Paragraph header
                If text = "" Then
                    text &= node.InnerText & vbCrLf
                Else
                    text &= vbCrLf & node.InnerText & vbCrLf
                End If

            ElseIf node.LocalName = "table" Then
                'Handle tables

                Dim tr As XmlNodeList = node.SelectNodes("*")
                Dim td As XmlNodeList
                Dim trNd, tdNd As XmlNode

                'Padding varies according to table type
                Dim lPad As Integer = 28
                Dim sPad As Integer = 4

                'loop through each <tr> and <td>
                For Each trNd In tr
                    td = trNd.SelectNodes("*")

                    'Control vars to get the columns right
                    Dim pad As Integer = 0
                    Dim sec As Integer = 1
                    For Each tdNd In td
                        If sec = 1 Then pad = lPad Else pad = sPad
                        text &= tdNd.InnerText.PadRight(pad)
                        sec += 1
                    Next

                    'New line
                    text &= vbCrLf
                Next
            Else
                'More optional tags handeled here
            End If

        Next 'End body handling

        'Print body parts
        If ingress <> "" Then output.Append("*IN:" & ingress)
        If by <> "" Then
            output.Append("*TE:" & by & text)
        Else
            output.Append("*TE:" & text)
        End If

        'Print article end marker
        output.Append(vbCrLf & vbCrLf & "*** BRS DOCUMENT BOUNDARY ***" & vbCrLf) ' & hdr.fileName

        Return output.ToString()
    End Function


End Class
